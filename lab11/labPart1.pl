% ES 1.1
 
% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst(?Elem,?List,?OutList)
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

% dropLast(?Elem,?List,?OutList)
dropLast(X,[H|Xs],[H|L]):-dropLast(X,Xs,L),!.
dropLast(X,[X|T],T).

% dropAll(?Elem,?List,?OutList)
%dropAll(X,[X|T],R):-dropAll(X,T,R),!.
%dropAll(X,[X|T],T).
%dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).

dropAll(X,[],[]).
dropAll(X,[X|T],L):-dropAll(X,T,L),!.
dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).

