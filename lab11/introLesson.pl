append([],L,L). % mette in relazione una lista vuota con L e L
% mette in relazione le liste L1,L2,L3 seL1 e L3 hanno stessa testa e se esiste la relazione L1',L2,L3' dove
% L1' e L3' sono code di L1 e L3.
append([I|Is],L,[I|Os]):-append(Is,L,Os). 

% Full relationality
% significa che il set di argomenti di un predicato possiamo utilizzarlo sia come input che ouput, risolvendo
% il goal si itera sulle possibili soluzioni, infinite. 
% Questo è fondamentale quando ci si affida alla composizione di goal.
% Tipicamente i predicati hanno vincoli sul set di I/O che supportano.
% La Full realtionality permette di astrarre dal contesto dove un goal è chiamato.

% member2(List, Elem, ListWithoutElem)
member2([X|Xs],X,Xs).
member2([X|Xs],E,[X|Ys]):-member2(Xs,E,Ys).
% permutation(Ilist, Olist) tutte le possibili combinazioni di valori nella prima lista passate
permutation([],[]).
permutation(Xs, [X | Ys]) :-
	member2(Xs,X,Zs),
	permutation(Zs, Ys).

% Generare combinazioni
% Prolog è utile per ottenere soluzioni in uno spazio di possibilità in maniera dinamica
interval(A,B,A).
interval(A,B,X):- A2 is A+1, A2<B, interval(A2,B,X).
neighbour(A,B,A,B2):- B2 is B+1.
neighbour(A,B,A,B2):- B2 is B-1.
neighbour(A,B,A2,B):- A2 is A+1.
neighbour(A,B,A,B2):- A2 is A-1.
%generate all links in a grid (NxM) of nodes
gridlink(N,M,link(X,Y,X2,Y2)):-
	interval(0,N,X),
	interval(0,M,Y),
	neighbour(X,Y,X2,Y2),
	X2>0,Y2>0,X2<N,Y2<M.

% esistono anche predicati non-relazionali inventati per migliorare l'espressività della programmazione
% migliori performance, miglior controllo dei meccanismi sottostanti, visione dei dati come programmi (meta programmazione)

% se ho bisogno di un predicato che ritorna tutte le permutazioni è elegante:
% fare un predicato che produce tutte le permutazioni una ad una
% e poi uso il predicato findall/3 che raccoglie tutte le soluzioni di un goal e le mette in una lista

% FINDALL
% trova tutte le soluzioni per un dato goal
% findall(OutTerm,Goal,OutList)
% map(List,Elem,OutElem,OutList)
% cambia tutti gli elementi in una lista allo stesso modo, anche per filtrare
map(L,E,OE,OL):-findall(OE,member(E,L),OL).


% cut
% merge(List1,List2,OutList)
% merge two sorted lists
merge(Xs,[],Xs).
merge([],Ys,Ys).
merge([X|Xs],[Y|Ys],[X|Zs]):-X<Y,merge(Xs,[Y|Ys],Zs).
merge([X|Xs],[Y|Ys],[X,Y|Zs]):-X=:=Y,merge(Xs,Ys,Zs).
merge([X|Xs],[Y|Ys],[Y|Zs]):-X>Y,merge([X|Xs],Ys,Zs).

% predicato ! di cut, sempre positivamente valutato
% le possibilità rimanenti aperte nel predicato corrente sarà interrotto

% merge(List1,List2,OutList)
% merge two sorted lists
merge(Xs,[],Xs):-!.
merge([],Ys,Ys).
merge([X|Xs],[Y|Ys],[X|Zs]):-X<Y,!,merge(Xs,[Y|Ys],Zs).
merge([X|Xs],[Y|Ys],[X,Y|Zs]):-X=:=Y,!,merge(Xs,Ys,Zs).
merge([X|Xs],[Y|Ys],[Y|Zs]):-merge([X|Xs],Ys,Zs).

% lookup([E|_],0,E).
% lookup([_|T],s(N),E) :- lookup(T,N,E).
lookup([E|_],0,E):- !.
lookup([_|T],s(N),E) :- lookup(T,N,E).

% quicksort (+Ilist,-Olist)
quicksort([],[]).
quicksort([X|Xs],Ys):-
	partition(Xs,X,Ls,Bs),
	quicksort(Ls,LOs),
	quicksort(Bs,BOs),
	append(LOs,[X|BOs],Ys).

% partition (Ilist,Pivot,Littles,Bigs)
partition([],_,[],[]).
partition([X|Xs],Y,[X|Ls],Bs):- X<Y, !, partition(Xs,Y,Ls,Bs).
partition([X|Xs],Y,Ls,[X|Bs]):- partition(Xs,Y,Ls,Bs).

% altri predicati interessanti
% var(X) controlla se l'argomento è una var
% nonvar(X) controlla se l'argomento non è una var
% ground(X) controlla se l'argomento non ha variabili internamente
% variazioni dell'uguaglianza:
% unify(X,Y) è uguale a X=Y    (\= l'opposto)
% X == Y ha successo se sono identici (\== opposto)
% X=:=Y: ha successo se X e Y sono numeri identici

% I/O Annotazioni per descrivere progremmi
% "-" output
% "+" input
% "@" input che dorebbero essere ground
% "?" sia input che output

% TREE
% functor tree/3 (left, node, right)

% leftlist(+Tree,-List), returns the left-most branch as a list
leftlist(nil,[]).
leftlist(tree(nil,E,_),[E]):-!.
leftlist(tree(T,E,_),[E|L]) :- leftlist(T,L). 

% search(+Tree,?Elem), search Elem in Tree
search(tree(_,E,_),E).
search(tree(L,_,_),E):- search(L,E).
search(tree(_,_,R),E):- search(R,E).

% leaves(+Tree,-ListLeaves), returns the list of leaves
leaves(nil,[]).									% gestione tree vuoto 
leaves(tree(nil,E,nil),[E]):-!.	% gestione foglia
leaves(tree(L,_,R),O):-					% caso generale
	leaves(L,OL),									% Foglie a sinistra
	leaves(R,OR),									% Foglie a destra
	append(OL,OR,O).							% O appends le due

% DB like structure
% X=[user(1001,mario,rossi),user(1002,luca,bianchi),...]

% Projection
% get_ids(+Table,-List)
% gets the List of ids from the Table
get_ids([],[]).
get_ids([user(ID,_,_)|T],[ID|L]):-get_ids(T,L).

% alternatively
get_ids(T,L):-map(T,user(ID,_,_),ID,L).

% QUERY
% query(+Table,+Id,-Tuple)
% gets the Tuple with Id from the Table
query([user(ID,N,C)|_],ID, user(ID,N,C)).
query([_|T],ID,Tuple):- query(T,ID,Tuple).

% update(+Table,+Id,+NewTuple,-NewTable)
% updates the tuple with Id to Tuple
update([user(ID,_,_)|T],ID,Tuple,[Tuple|T]).
update([H|T],ID,Tuple,[H|Table]):-update(T,ID,Tuple,Table).

% factorial(+N,-Out,?Cache)
% cache is a partial cache of factorials [1,1,2,6,24|_]
factorial(N,Out,Cache):-factorial(N,Out,Cache,0).
factorial(N,Res,[Res|_],N):-!,nonvar(Res).
factorial(N,Out,[H,V|T],I):-
	var(V), !, I2 is I+1, V is H*I2,
	factorial(N,Out,[V|T],I2).
factorial(N,Out,[_,V|T],I):-
	I2 is I+1, factorial(N,Out,[V|T],I2).

% TAIL RECURSIVE

% count(+List,+Elem,-Num)
% counts how many occurrences of Elem exist in List
count([],_,0).
count([E|T],E,N) :- !, count(T,E,N2), N is N2+1.
count([_|T],E,N) :- count(T,E,N).

% count(+List,+Elem,-Num)
% counts how many occurrences of Elem are in List
count(L,E,N) :- count(L,E,0,N).
count([],_,N,N).
count([E|T],E,N,O) :- !, N2 is N+1, count(T,E,N2,O).
count([_|T],E,N,O) :- count(T,E,N,O).






