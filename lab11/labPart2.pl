dropAll(X,[],[]).
dropAll(X,[X|T],L):-dropAll(X,T,L),!.
dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).

% ES 2.1

% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% ES 2.2
% fromCircList(+List,-Graph)
% which implementation?
% fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
fromCircList(F,[L],[e(L,F)]).
fromCircList(F, [H1,H2|T], [e(H1,H2)|L]):-fromCircList(F, [H2|T],L).
fromCircList([H1,H2|T],[e(H1,H2)|L]):- fromCircList(H1,[H2|T],L).

% ES 2.3
% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
% dropNode([e(1,2),e(1,3),e(2,3)],1,[e(2,3)]).
% 
dropNode(G,N,O):- 
	dropAll(e(N,_),G,G2), 
	dropAll(e(_,N),G2,O).

% ES 2.4
% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)
% TEST:
% reaching([e(1,2),e(1,3),e(2,3)],1,L).
reaching(G,N,L):-findall(S,member(e(N,S),G),L).

% NOTE
% MEMBER/2 True if Elem is a member of List
% FINDALL/3 (object, goal, list) -> produces a list of all the objects that satisfy the goal.

% ES 2.5
% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
% HINT
% a path from N1 to N2 exists if there is a e(N1,N2)
% a path from N1 to N2 is OK if N3 can be reached from N1, and then there is a path from N2 to N3, recursively
anypath(G,N1,N2,[e(N1,N3)|L]):- member(e(N1,N3),G), anypath(G,N3,N2,L).
anypath(G,N1,N2,[e(N1,N2)]):- member(e(N1,N2),G),!.

% ES 2.6
% allreaching(+Graph, +Node, -List) 
% all the nodes that can be reached from Node 
% Suppose the graph is NOT circular! 
% Use findall and anyPath!
allreaching(G,N,L):-findall(S, anypath(G,N,S,_), L).
