% Implement predicate next/4 as follows
% � next(@Table,@Player,-Result,-NewTable)
% � Table is a representation of a TTT table where 
% players x or o are playing
% � Player (either x or o) is the player to move
% � Result is either (either win(x), win(o), nothing, or even)
% � NewTable is the table after a valid move
% � Should find a representation for the Table
% � Calling the predicate should give all results
% Secondarily, implement predicate:
% � game(@Table,@Player,-Result,-TableList)
% � TableList is the sequence of tables until Result win(x),
% win(o) or even

% � [[_,_,_],[x,o,x],[o,x,_]]: nice but advanced
% � [[n,n,n],[x,o,x],[o,x,n]]: compact, but need work
% � [cell(0,1,x),cell(1,1,o),cell(2,1,x),�]: easier

% goal: next([cell(0,0,e),cell(0,1,e),cell(0,2,e),cell(1,0,e),cell(1,1,e),cell(1,2,e),cell(2,0,e),cell(2,1,e),cell(2,2,e)], x, R, NT).

% ------------------ check - pattern (+board, +player, -result) ------------------- %
% ex: check([cell(0,0,x),cell(0,1,o),cell(0,2,x)],x,X).

% ROW
check(NB,P,win(P)):-member(cell(0,0,P),NB),member(cell(0,1,P),NB),member(cell(0,2,P),NB),!.
check(NB,P,win(P)):-member(cell(1,0,P),NB),member(cell(1,1,P),NB),member(cell(1,2,P),NB),!.
check(NB,P,win(P)):-member(cell(2,0,P),NB),member(cell(2,1,P),NB),member(cell(2,2,P),NB),!.
% COLUMN
check(NB,P,win(P)):-member(cell(0,0,P),NB),member(cell(1,0,P),NB),member(cell(2,0,P),NB),!.
check(NB,P,win(P)):-member(cell(0,1,P),NB),member(cell(1,1,P),NB),member(cell(2,1,P),NB),!.
check(NB,P,win(P)):-member(cell(0,2,P),NB),member(cell(1,2,P),NB),member(cell(2,2,P),NB),!.
% DIAG
check(NB,P,win(P)):-member(cell(0,0,P),NB),member(cell(1,1,P),NB),member(cell(2,2,P),NB),!.
check(NB,P,win(P)):-member(cell(0,2,P),NB),member(cell(1,1,P),NB),member(cell(2,0,P),NB),!.
% EVEN
check(NB,P,nothing):-member(cell(X,Y,e),NB),!.
% NOTHING
check(NB,P,even).

% ------------------ move - pattern (+board, +player, -newBoard) ------------------- %

move(B,P,NB):-
	member(cell(X,Y,e),B),
	replace(B,cell(X,Y,e),cell(X,Y,P),NB).

replace([H|T], H, N, NT):- 
    NT = [N|T].
replace([H|T], O, N, [H|TT]):-
    replace(T, O, N, TT).

% ------------------ next - pattern (+board, +player, -result, -newBoard) ------------------- %

next(T,P,R,NT):-
	move(T,P,NT),
	check(NT,P,R).

% ------------------ switchTurn - pattern (oldPlayer, newPlayer) ------------------- %

switchTurn(x,o).
switchTurn(o,x).

% ------------------ game - pattern (+board, +player, -result, -boardsList) ------------------- %

game(B,P,R,BL):-game(B,P,R,nothing,BL,[]).

game(_,_,R,RR,BL,T):-
	RR \= nothing,
	R = RR,
	BL = T.

game(B,P,R,RR,BL,T):-
	findall((NR,NB),next(B,P,NR,NB),L),
	member((X,Y),L),
	append(T,[Y],NL),
	switchTurn(P,NP),
	game(Y,NP,R,X,BL,NL).


