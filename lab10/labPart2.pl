% size(List,Size) 
% Size will contain the number of elements in List 
size([],0). 
size([_|T],M) :- size(T,N), M is N+1.

% size(List,Size) 
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..
size2([], zero).
size2([_|T], M):-size2(T, N), M = s(N).

% sum(List,Sum)
sum([],0).
sum([X|Xs], S):-sum(Xs, P), S is P+X.

% average(List,Average)
% it uses average(List,Count,Sum,Averag)
average(List,A):-average(List, 0, 0, A).
average([], C, S, A) :- A is S/C.
average([X|Xs],C,S,A) :-
	C2 is C+1,
	S2 is S+X,
	average(Xs,C2,S2,A).

% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max(List, M):-max(List, M, 0).
max([], M, TM):- M is TM.
max([X|Xs], M, TM):-
	X > TM,
	max(Xs, M, X).
max([X|Xs], M, TM):-
	X < TM,
	max(Xs, M, TM). 











