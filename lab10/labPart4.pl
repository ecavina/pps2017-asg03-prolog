% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[X]):- X = 0.
seqR(N,[X|Xs]):- X = N, N2 is N-1, seqR(N2, Xs).

% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).

% senza last
seqR2(N, List):-seqR2(0, N, List).
seqR2(C, 0, [X]):- C = X.
seqR2(C, N, [X|Xs]):- 
	C = X, 
	N2 is N-1,
	C2 is C+1,
	seqR2(C2, N2, Xs). 

% con last
last([], N, [N]).
last([H|T], N, [H|R]) :- last(T, N, R).

seqRL2(0, [0]).
seqRL2(N, List):- last(CuttedList, N, List), N2 is N-1, seqRL2(N2, CuttedList).

% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv([],[]).
inv(IncList, [DecH|DecT]):- last(ResultIncList, DecH, IncList), inv(ResultIncList, DecT).

% double(List,List) 
% suggestion: remember predicate append/3 
% example: double([1,2,3],[1,2,3,1,2,3]). 
double(FirstList, SecondList):- append(FirstList, FirstList, SecondList).

% times(List,N,List) 
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]). 
times(FirstList, N, MultipliedList):- times(FirstList, N, FirstList, MultipliedList).
times(BaseList, 1, MultipliedList, MultipliedList).
times(BaseList, N, CurrentList, MultipliedList):-
	append(BaseList, CurrentList, ResultList),
	N2 is N-1,
	times(BaseList, N2, ResultList, MultipliedList).
	
% proj(List,List) 
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj([[NH1,_]|Rest], [H2|T2]):- NH1 = H2, proj(Rest,T2).






















