
% program as �facts�

% fino al . � un fatto, composto da predicato(term1, term2, ...)

% il numero di termini � l'arit�

% i termini possono essere numeri o costanti

% i goals sono query sui fatti

% query con variabili -> esistenziali (variabili maiuscole)

% compound term: un term con una struttura dati non atomica, es:
% manager(person(john, smith, 1283)) -> person � un functor o compound term

% universal fact: fatti con variabili

% substitution: � una mappa finita da variabili a termini: plus(1,X,2){X/1}

% unificazione e mgu: mgu(a(1,2),a(X,Y))={X/1,Y/2}

% fact: mgu(Term,Term). goal: mgu(a(1,2),a(X,Y)). ? yes. X/1, Y/2, solution mgu(a(1,2),a(1,2))

% i programmi dovrebbero usare l'identificatore di variabile "fresh" 
% se non bisogna indicare pi� occorrenze della stessa variabile

% si possono congiungere i goal con la ","

% RULES fact :- body  > il fatto � tale se il body pu� essere risolto 

% la somma di X e il successore di Y � il successore di Z se la somma
% di X e Y fa Z
sum(X,zero,X). 
sum(X,s(Y),s(Z)):-sum(X,Y,Z).
% X * successore di Y = Z se X * Y = W e la W + X = Z
mul(X,zero,zero). 
mul(X,s(Y),Z):-mul(X,Y,W),sum(W,X,Z).

dec(s(X),X). 
factorial(zero,s(zero)). 
factorial(s(X),Y):-factorial(X,Z), mul(s(X),Z,Y).

%search(cons(E,_),E). 
%search(cons(H,T),E) :- H \= E, search(T,E).
% simile ma risponde per ogni presenza
search(cons(E,_),E). 
search(cons(_,T),E) :- search(T,E).

% Lists in Prolog
% Cons(h,t) = [H|T]
% nill = []
% Cons(H1, Cons(H2, nill)) = [H1, H2]
% Cons(H1, Cons(H2, T)) = [H1, H2 | T]
search([E|_], E).
search([_|T], E) :- search(T, E).

% mette nel secondo argomento la posizione del terzo all'interno del primo
lookup([E|_],zero,E). 
lookup([H|T],s(N),E) :- lookup(T,N,E).

% concatena la prima con la seconda lista il cui risultato � il terzo elemento  
append([],L,L). 
append([H|T],L,[H|M]):- append(T,L,M).

% ARITHMETICS
% Numeri: sono visti come costanti speciali
% Operatori: sono definiti come functor Prolog,'+'(1,2) ma si pu� fare anche 1+2
% Prolog fornisce un predicato "is/2", il primo parametro � la variabile o numero, il secondo � l'espressione
% es:  4 is 1+3. 
% altri predicati build in: 
% =:= equivalenza
% =\= diversit�
% >, <, >=, =<
% = unificare due termini, /= opposto
% Nota: =/2 � per unificare mentre is/2 � per calcolare espressioni

% size(List,Size) 
% Size will contain the number of elements in List
size([],0).
size([_|T],M):-size(T,N), M is N+1.

% comportamento veramente relazionale, 
% questo predicato fa altre cose oltre che cercare e basta in una lista
% corrisponde al member/2 build-in in Prolog
search([E|_],E). 
search([_|T],E) :- search(T,E).

% member as part of the library member([H|T],H,T). 
member([H|T],E,[H|T2]):- member(T,E,T2). 
permutation([],[]). 
permutation(L,[H|TP]) :- member(L,H,T), permutation(T,TP).

quicksort([],[]). 
quicksort([X|Xs],Ys):- partition(Xs,X,Ls,Bs), 
												quicksort(Ls,LOs), 
												quicksort(Bs,BOs), 
												append(LOs,[X|BOs],Ys). 
% partition as part of the library 
partition([],_,[],[]). 
partition([X|Xs],Y,[X|Ls],Bs):- X<Y, !, partition(Xs,Y,Ls,Bs). 
partition([X|Xs],Y,Ls,[X|Bs]):- partition(Xs,Y,Ls,Bs).